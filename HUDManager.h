#pragma once

#include <opencv2/opencv.hpp>

using namespace cv;

class HUDManager
{
    public:
        HUDManager();
        ~HUDManager() = default;
        
        
        static void insert_at(Mat& src, Mat& dst, int offset_x, int offset_y, Vec3b src_mask_color, Vec3b dst_mask_color);


        static void filter_central_circle(Mat& m);


        static void rotate(Mat& src, double angle, Mat& dst);


        static Mat create_gyro(int roll_val_degrees, int pitch_val_degrees);


        static Mat create_compass(int yaw_degrees, Point2f local_goal = Point2f());

        static Mat create_steering(double angle);

    private:
        static Mat _roll, _template, _static, _bar, _selector, _steering;
};



