/*
 * BaseException.cpp
 *
 *  Created on: Jan 26, 2018
 *      Author: roma
 */

#include "Exception.hpp"


Exception::Exception(std::string msg)
{
    _message = msg;
}

Exception::Exception(const Exception& other) : _message(other._message)
{
}

Exception& Exception::operator=(const Exception& other)
{
    if(this != &other)
    {
        _message = other._message;
    }
    return *this;
}

const char* Exception::what() const noexcept
{
    return _message.c_str();
}
