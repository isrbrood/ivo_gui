#pragma once
#include "NonCopyable.hpp"


class JoystickCmd{
public:
    void print_values()
    {
        std::cout << drive_mode << " | g1: " << gas_cmd << "  g2: " << gas2_cmd << ", b: " << brake_cmd << ", s: " << steering_cmd << std::endl;
    }

    float gas_cmd, brake_cmd, steering_cmd, gas2_cmd;
    int drive_mode, start, reset_enc, x2_steer, is_change;
    /* Buttons index */
    const static int DRIVE_MODE_IDLE = 0;
    const static int DRIVE_MODE_MANUAL_INPUT = 2;
    const static int DRIVE_MODE_REMOTE_INPUT = 1;
    const static int DRIVE_MODE_AUTONOMOUS_INPUT = 3;
    const static int RESET_ENCODER_BUTTON = 5;
    const static int START_BUTTON = 7;
    const static int X2STEER = 4;


};


class OdomMsg final : public NonCopyable
{
    public:
        OdomMsg(double r, double p, double y, double linear_speed) :
            _r(r), _p(p), _y(y), _speed(linear_speed)
        {

        }

        // odom + speed
        double _r, _p, _y; // orientation
        double _speed;
};








