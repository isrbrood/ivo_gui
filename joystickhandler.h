#ifndef JOYSTICKHANDLER_H
#define JOYSTICKHANDLER_H

#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/joystick.h>
#include <time.h>
#include <string>
#include <thread>
#include "MessageTypes.hpp"
#define JOY_DEV "/dev/input/js"
#define AXES_MULTIPLIER 1.0 / 32767.0



class JoystickHandler
{
public:
    JoystickHandler(std::string joy_name);
    ~JoystickHandler();
    void loop();
    void kill();
    void run();
    JoystickCmd get_joystick_cmd();

private:
    int joy_fd, axis[10] = {0}, num_of_axis = 0, num_of_buttons = 0;
    char button[100] = {0} , name_of_joystick[80];
    bool alive;
    std::thread _loop_t;
    struct js_event js;
    JoystickCmd jc;
};

#endif // JOYSTICKHANDLER_H
