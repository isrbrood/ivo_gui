#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ui_ivo_main.h"
#include <opencv2/opencv.hpp>
#include <thread>
#include "HUDManager.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    Q_SIGNALS:
    void updateTopLeftText(QString);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void resizeEvent(QResizeEvent* event);

signals:
    void do_connect(const QString& ip);

private slots:
    void _do_connect();
    void set_top_left_text(const QString& str);
    void set_image(const QVector<unsigned char>& img);
    void set_orientation(const QVector<double>& rpy);
    void set_speed(double speed);

private:
    void _clear_canvas();
    void worker_thread();

    Ui::IvoMainScreen *     _ui;
    QGraphicsScene*         _scene;
    QGraphicsPixmapItem*    _image;
    QGraphicsTextItem*      _top_left;
    QGraphicsTextItem*      _top_right;
    QGraphicsPixmapItem*    _gyro, *_compass;
    QGraphicsLineItem*      _lineH, *_lineOrg;
    HUDManager              _hud_manager;

};

#endif // MAINWINDOW_H
