#-------------------------------------------------
#
# Project created by QtCreator 2018-06-08T17:03:13
#
#-------------------------------------------------

QT       += core gui
QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ivo_gui
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    ivogui.cpp \
    netlib.cpp \
    joystickhandler.cpp \
    Exception.cpp \
    HUDManager.cpp

HEADERS += \
        mainwindow.h \
    ivogui.hpp \
    netlib.h \
    joystickhandler.h \
    Exception.hpp \
    HUDManager.h \
    MessageTypes.hpp

FORMS += \
    ivo_main.ui

LIBS += -L/opt/ros/kinetic/lib/x86_64-linux-gnu/ -lopencv_core3 -lopencv_imgproc3 -lopencv_imgcodecs3 -lopencv_highgui3

INCLUDEPATH += /opt/ros/kinetic/include/opencv-3.3.1-dev/

DISTFILES += \
    img/bar.png \
    img/gyro_roll.png \
    img/gyro_static.png \
    img/gyro_template.png \
    img/selector.png \
    img/steering.png
