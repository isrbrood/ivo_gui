#include "ivogui.hpp"
#include <functional>
#include "MessageTypes.hpp"

#define IVO_SERVER_PORT 28004
#define IVO_CLIENT_PORT 28005

IvoGUI::IvoGUI(const MainWindow& window) :
    _conneciton(IVO_CLIENT_PORT),
    _main_thread(std::thread(std::bind(&IvoGUI::_main_thread_func, this))),
    _receiver_thread(std::thread(std::bind(&IvoGUI::_receiver_thread_func, this))),
    _should_stop(false),
    _connected(false),
    _receive_traffic_accu(0),
    _receive_images_accu(0),
    _joystick_handler(JOY_DEV)
{
    qRegisterMetaType<QVector<unsigned char> >("QVector<unsigned char>");
    qRegisterMetaType<QVector<double> >("QVector<double>");
    connect(this, SIGNAL(set_top_left_text(const QString&)), &window, SLOT(set_top_left_text(const QString&)));
    connect(this, SIGNAL(set_speed(double)), &window, SLOT(set_speed(double)));
    connect(this, SIGNAL(set_image(const QVector<unsigned char>&)),&window, SLOT(set_image(const QVector<unsigned char>&)));
    connect(this, SIGNAL(set_orientation(const QVector<double>&)),&window, SLOT(set_orientation(const QVector<double>&)));
    connect(&window, SIGNAL(do_connect(const QString&)), this, SLOT(do_connect(const QString&)));
    //_joystick_handler.run();
}

IvoGUI::~IvoGUI()
{
    _should_stop = true;
    _main_thread.join();
}

void IvoGUI::_main_thread_func()
{
    static const uint FREQUENCY = 30; //Hz
    int time_accu = 0;
    std::this_thread::sleep_for(std::chrono::seconds(1)); // bug fix (race with _should_stop initialization)

    while(!_should_stop)
    {
        if(_connected)
        {

            _joystick_cmd = _joystick_handler.get_joystick_cmd();

            PacketPtr packet = Packet::make(sizeof(JoystickCmd));
            packet->type = PacketType::JOYSTICK;
            memcpy(&packet->data, &_joystick_cmd, sizeof(JoystickCmd));
            _conneciton.send(*packet, sizeof(JoystickCmd));

            if(time_accu >= 1000)
            {
                char bw[80];
                sprintf(bw, "BW Usage: %.2f Mbit/s\nFPS: %d", _receive_traffic_accu/128.0/1024, _receive_images_accu);
                emit set_top_left_text(QString(bw));
                time_accu = 0;
                _receive_traffic_accu = 0;
                _receive_images_accu = 0;
            }

        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000/FREQUENCY));
        time_accu += 1000/FREQUENCY;
    }
}

void IvoGUI::_receiver_thread_func()
{
    while(!_should_stop)
    {
        std::vector<uchar> data;
        _conneciton.receive(data);
        _connected = true;

        _receive_traffic_accu += data.size();

        Packet* pkt = (Packet*)data.data();
        std::vector<uchar> payload = pkt->get_data(data.size());

        cv::Mat mat;
        QVector<unsigned char> vec;

        switch(pkt->type)
        {
            case PacketType::HELLO:
                break;
            case PacketType::IMAGE:
                _receive_images_accu++;
                mat = cv::imdecode(std::vector<unsigned char>(payload.begin(), payload.end()), CV_LOAD_IMAGE_COLOR);
                cv::cvtColor(mat, mat, CV_BGR2RGB);
                vec.reserve(mat.step*mat.rows);
                std::copy(mat.data, mat.data+mat.step*mat.rows, std::back_inserter(vec));
                emit set_image(vec);
                break;

            case PacketType::DRIVE_MODE:
                emit set_top_left_text((char*)(pkt->data));
                break;

            case PacketType::ODOM:
                //OdomMsg* msg = (OdomMsg*)payload.data();
                emit set_orientation({((OdomMsg*)payload.data())->_r, ((OdomMsg*)payload.data())->_p, ((OdomMsg*)payload.data())->_y});
                emit set_speed(((OdomMsg*)payload.data())->_speed);
                break;
            default:
                break;
        }
    }
}

void IvoGUI::do_connect(const QString &ip)
{
    _conneciton.connect(ip.toAscii().constData(), IVO_SERVER_PORT);
    Packet p;
    p.type = PacketType::HELLO;

    _conneciton.send(p, 0);
    _connected = true;
}
