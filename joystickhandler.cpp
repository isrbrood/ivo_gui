#include "joystickhandler.h"
#include "MessageTypes.hpp"

JoystickHandler::JoystickHandler(std::string joy_name)
{
    for (int i = 0; i < 5; i++)
    {
        std::string name = "/dev/input/js" + std::to_string(i);
    	if((joy_fd = open(name.c_str(),O_RDONLY))== -1 )
    	{
            if (i < 3)
                continue;
            printf(" couldn't open the joystick \n " );

            return;
    	}
        break;
    }
    ioctl(joy_fd, JSIOCGAXES , &num_of_axis);
    ioctl(joy_fd, JSIOCGBUTTONS , &num_of_buttons);
    ioctl(joy_fd, JSIOCGNAME(80), &name_of_joystick);

    printf( " Joy stick detected : %s \n \t %d axis \n\t %d buttons \n\n" ,name_of_joystick , num_of_axis , num_of_buttons);
    fcntl( joy_fd, F_SETFL , O_FSYNC); // use non - blocking methods
    alive = true;
}

JoystickHandler::~JoystickHandler()
{

    kill();
}

void JoystickHandler::run()
{
    _loop_t = std::thread(&JoystickHandler::loop, this);
}

void JoystickHandler::kill()
{
    if (!alive)
        return;
    alive = false;
    std::cout << "Waitting for thread to finish\n";
    _loop_t.join();
    std::cout << "Thread finished\n";
    close(joy_fd);
}

void JoystickHandler::loop()
{
    while(alive) // infinite loop

    {
        // read the joystick

        read (joy_fd, &js , sizeof(struct js_event));

        // see what to do with the event

        switch(js.type & ~ JS_EVENT_INIT)
        {
            case JS_EVENT_AXIS :

                axis [ js.number ] = js.value;
                break;

            case JS_EVENT_BUTTON :

                button [js.number ] = js.value;
                break;
        }
    }
        // print the results
}


JoystickCmd JoystickHandler::get_joystick_cmd()
{
    /* Axis */
    jc.steering_cmd = AXES_MULTIPLIER * axis[0];
    jc.brake_cmd = AXES_MULTIPLIER * axis[4];
    jc.gas_cmd = AXES_MULTIPLIER * axis[2] + 1;
    jc.gas2_cmd = AXES_MULTIPLIER * axis[5] + 1;
    /* Buttons */
    if (button[jc.DRIVE_MODE_IDLE])
        jc.drive_mode = jc.DRIVE_MODE_IDLE;
    else if (button[jc.DRIVE_MODE_MANUAL_INPUT])
        jc.drive_mode = jc.DRIVE_MODE_MANUAL_INPUT;
    else if (button[jc.DRIVE_MODE_REMOTE_INPUT])
        jc.drive_mode = jc.DRIVE_MODE_REMOTE_INPUT;
    else if (button[jc.DRIVE_MODE_AUTONOMOUS_INPUT])
        jc.drive_mode = jc.DRIVE_MODE_AUTONOMOUS_INPUT;

    jc.is_change = button[jc.DRIVE_MODE_IDLE] + button[jc.DRIVE_MODE_MANUAL_INPUT] + button[jc.DRIVE_MODE_REMOTE_INPUT] + button[jc.DRIVE_MODE_AUTONOMOUS_INPUT];
    jc.reset_enc = button[jc.RESET_ENCODER_BUTTON];
    jc.start = button[jc.START_BUTTON];
    jc.x2_steer = button[jc.X2STEER];

    return jc;
}
