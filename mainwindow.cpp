#include "mainwindow.h"
#include <QGraphicsTextItem>
#include <QGraphicsPixmapItem>
#include <QMessageBox>

#define TRANSMITTER_IMAGE_WIDTH     600
#define TRANSMITTER_IMAGE_HEIGHT    480
#define CAMERA_FOV_VERTICAL_DEG     80.0


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    _ui(new Ui::IvoMainScreen),
    _scene(new QGraphicsScene)
{
    _ui->setupUi(this);
    connect(_ui->ConnectButton, SIGNAL (released()),this, SLOT (_do_connect()));

    _ui->videoCanvas->setScene(_scene);
    QPixmap pixMap = QPixmap(TRANSMITTER_IMAGE_WIDTH,TRANSMITTER_IMAGE_HEIGHT);
    pixMap.fill(Qt::darkBlue);
    _image = _scene->addPixmap(pixMap);
    //this->setStyleSheet("background-color: #ccccff;");
    QFont serif("Courier New",22,QFont::Bold); // serif is a variable, change later
    _top_left = _scene->addText(QString("---"));
    _top_left->setFont(serif);
    _top_left->setDefaultTextColor(Qt::red);

    _top_right = _scene->addText(QString("---"));
    _top_right->setFont(serif);
    _top_right->setDefaultTextColor(Qt::magenta);

    _gyro = _compass = nullptr;
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
   QMainWindow::resizeEvent(event);
   _ui->videoCanvas->setFixedSize(size());
   // printf("%d %f\n", _ui->videoCanvas->height(), _image->scene()->height());
   _image->scale(_ui->videoCanvas->width() / _image->scene()->width(), (_ui->videoCanvas->height() - 20) / _image->scene()->height());
   _top_right->setPos(_ui->videoCanvas->width()-300, _top_left->pos().y());

   setMinimumSize(size());
}

MainWindow::~MainWindow()
{
    delete _scene;
    delete _ui;
}

void MainWindow::_do_connect()
{
    emit do_connect(_ui->editIpAddr->text());
}

void MainWindow::set_top_left_text(const QString &str)
{
    _top_left->setPlainText(str);
}

void MainWindow::set_image(const QVector<unsigned char>& vec)
{
    cv::Mat mat(TRANSMITTER_IMAGE_HEIGHT, TRANSMITTER_IMAGE_WIDTH, CV_8UC3, (void*)vec.data());
    QPixmap map = QPixmap::fromImage(QImage((unsigned char*) mat.data, mat.cols, mat.rows, QImage::Format_RGB888));
    _image->setPixmap(map);
}

void MainWindow::set_speed(double speed)
{
    char buf[250];
    sprintf(buf, "Speed: %.2f[Km/H]", abs(speed)*3.6);
    _top_right->setPlainText(buf);
}

void MainWindow::set_orientation(const QVector<double>& rpy)
{
    double r = rpy[0], p = rpy[1], y = rpy[2];
    cv::Mat steering_mat = _hud_manager.create_steering(0); //create_gyro(r*180/M_PI, p*180/M_PI);
    cv::Mat compass_mat = _hud_manager.create_compass(y*180/M_PI, Point2f(0,0));

    QImage img_steer((unsigned char*) steering_mat.data, steering_mat.cols, steering_mat.rows, steering_mat.step, QImage::Format_ARGB32);
    QPixmap map_steer = QPixmap::fromImage(img_steer, Qt::AutoColor);

    QImage img_compass((unsigned char*) compass_mat.data, compass_mat.cols, compass_mat.rows, compass_mat.step, QImage::Format_RGB888);
    QPixmap map_compass = QPixmap::fromImage(img_compass, Qt::AutoColor);



    if(_gyro == nullptr)
    {
        _gyro = _scene->addPixmap(map_steer);
        _gyro->setPos(_ui->videoCanvas->width()-200, _top_right->pos().y()+100);

        _compass = _scene->addPixmap(map_compass);
        _compass->setPos(_ui->videoCanvas->width()/2-compass_mat.cols/2, _top_right->pos().y()+30);

        _lineH = _scene->addLine(0, _ui->videoCanvas->height()/2, _ui->videoCanvas->width(), _ui->videoCanvas->height()/2, QPen(Qt::cyan));
        _lineOrg = _scene->addLine(0, _ui->videoCanvas->height()/2, _ui->videoCanvas->width(), _ui->videoCanvas->height()/2, QPen(Qt::darkCyan));
    }
    else
    {
        _gyro->setPixmap(map_steer);
        _compass->setPixmap(map_compass);
        double pitch_angle_deg = p * 180 / M_PI;

        int new_h = _ui->videoCanvas->height() / 2 +
                _ui->videoCanvas->height() / 2 *
                pitch_angle_deg /
                CAMERA_FOV_VERTICAL_DEG / 2;

        int extra_y_from_roll = tan(r) * _ui->videoCanvas->width()/2;


        _lineH->setLine(0, new_h - extra_y_from_roll, _ui->videoCanvas->width(), new_h + extra_y_from_roll);

        _lineOrg->setLine(0, _ui->videoCanvas->height()/2, _ui->videoCanvas->width(), _ui->videoCanvas->height()/2);



    }
}
