#pragma once
#include <string>
#include <vector>
#include <netinet/in.h>
#include <iostream>
#include <memory>

#define CURRENT_NETLIB_VERSION  ((uint8_t) 2 )  // UPDATE THIS IF YOU MAKE MODS TO THE PROTOCOL

enum class PacketType : unsigned char
{
    HELLO,
    DRIVE_MODE, // null-terminated string
    IMAGE, // jpeg encoded cv::Mat
    JOYSTICK, // serialized JoystickCmd
    DM_STATE, // null-terminated string
    ODOM, // OdomMsg

};

class Packet;

typedef std::shared_ptr<Packet> PacketPtr;


class Packet
{
    public:
        Packet() : version(CURRENT_NETLIB_VERSION), type(PacketType::HELLO) {}

        static PacketPtr make(uint payload_size);
        static PacketPtr make(PacketType type, void* data, uint payload_size);

        std::vector<uint8_t> get_data(uint total_size) const;

    private:
        uint8_t         version;
    public:
        PacketType      type;
        unsigned char   data[];
        static uint8_t  packet_counter;
}
__attribute__((packed));


class Connection
{
public:
    Connection(u_short port);
    ~Connection();

    void connect(const std::string &dest, u_short port);
    bool is_connected() const;

    void send(const std::vector<u_char> &data);
    void send(const Packet &data, uint data_size);
    void receive(std::vector<u_char> &data);

private:
    int sockfd;
    struct sockaddr_in dest_addr;
};
