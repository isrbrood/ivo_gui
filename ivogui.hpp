#pragma once

#include <QMainWindow>
#include <mutex>
#include <QThread>
#include <thread>
#include <opencv2/opencv.hpp>

#include "mainwindow.h"
#include "netlib.h"
#include "joystickhandler.h"

class IvoGUI : public QThread
{
    Q_OBJECT

    public:
        IvoGUI(const MainWindow& window);
        virtual ~IvoGUI();

    signals:
        void set_top_left_text(const QString& str);
        void set_speed(double speed);
        void set_orientation(const QVector<double>& rpy);
        void set_image(const QVector<unsigned char>& img);
        void set_joystick_cmd(const QVector<unsigned char>& data);

    public slots:
       void do_connect(const QString& ip);

    private:
        Connection      _conneciton;
        std::thread     _main_thread;
        std::thread     _receiver_thread;
        bool            _should_stop;
        bool            _connected;

        int             _receive_traffic_accu;
        int             _receive_images_accu;

        void            _main_thread_func();
        void            _receiver_thread_func();


        JoystickHandler _joystick_handler;
        JoystickCmd     _joystick_cmd;
};
