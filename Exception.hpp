/*
 * BaseException.hpp
 *
 *  Created on: Jan 26, 2018
 *      Author: roma
 */

#pragma once

#include <exception>
#include <string>

class Exception : public std::exception
{
    public:
        Exception(const std::string msg);
        Exception() = default;
        ~Exception() = default;

        Exception(const Exception&);
        Exception& operator=(const Exception&);

        const char* what() const noexcept override;

    private:
        std::string     _message;
};


