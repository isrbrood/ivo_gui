#include "mainwindow.h"
#include <QApplication>
#include "ivogui.hpp"


int main(int argc, char *argv[])
{
    printf("ARGV: %s\n", argv[0]);
    QApplication a(argc, argv);
    MainWindow w;
    w.showMaximized();
    IvoGUI gui(w);

    return a.exec();
}
