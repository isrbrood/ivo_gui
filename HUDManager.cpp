#include "HUDManager.h"
#include <exception>
Mat HUDManager::_roll;
Mat HUDManager::_template;
Mat HUDManager::_static;
Mat HUDManager::_bar;
Mat HUDManager::_selector;
Mat HUDManager::_steering;

HUDManager::HUDManager()
{
    _roll = imread("img/gyro_roll.png", 1);
    _template = imread("img/gyro_template.png", 1);
    _static = imread("img/gyro_static.png", 1);
    _bar = imread("img/bar.png", 1);
    _selector = imread("img/selector.png", 1);
    _steering = imread("img/steering.png", -1);

    if(!_roll.data || !_template.data || !_static.data || !_selector.data || !_bar.data || !_steering.data)
    {
        printf("Couldn't load the HUD image files!\n");
        throw std::exception();
    }
    resize(_steering, _steering, Size(_steering.cols/6, _steering.rows/6));
}

void HUDManager::insert_at(Mat& src, Mat& dst, int offset_x, int offset_y, Vec3b src_mask_color, Vec3b dst_mask_color)
{
    for(int i = 0; i < src.rows; i++)
        for(int j = 0; j < src.cols; j++)
        {
            Vec3b pxl = src.at<Vec3b>(i,j);
            //if(norm(src_mask_color - pxl) < 1)
            if(norm(src_mask_color, pxl) < 120)
                continue;
            int pos_y = i+offset_y-src.rows/2;
            int pos_x = j+offset_x-src.cols/2;
            if(pos_y < 0 || pos_y >= dst.rows || pos_x < 0 || pos_x > dst.cols)
                continue;
            Vec3b& tgt = dst.at<Vec3b>(pos_y, pos_x);
            //if(tgt == dst_mask_color)
            //  continue;
            tgt = pxl;
        }
}

Mat HUDManager::create_gyro(int roll_val_degrees, int pitch_val_degrees)
{
    Mat result(_roll.size(), CV_8UC3, Scalar(0xff,0,0));
    Mat roll = _roll.clone();
    Mat temp = _template.clone();
    rotate(temp, roll_val_degrees, temp);
    rotate(roll, roll_val_degrees, roll);
    insert_at(temp, result, roll.cols/2, roll.rows/2+pitch_val_degrees*2.3, Vec3b(0,0,0),Vec3b(0,0,0));
    insert_at(_static, result, roll.cols/2, 63, Vec3b(0,0,0),Vec3b(0,0,0));
    insert_at(roll, result, result.cols/2, result.rows/2, Vec3b(0, 0, 0xff), Vec3b(0,0,0));
    filter_central_circle(result);
    return result;
}

Mat HUDManager::create_steering(double angle)
{
   Mat dst = _steering.clone();
   rotate(_steering, angle, dst);
   return dst;
}

void HUDManager::rotate(Mat& src, double angle, Mat& dst)
{
    cv::Point2f ptCp(src.cols*0.5, src.rows*0.5);
    cv::Mat M = cv::getRotationMatrix2D(ptCp, angle, 1.0);
    cv::warpAffine(src, dst, M, src.size(), cv::INTER_CUBIC); //Nearest is too rough,
}

void HUDManager::filter_central_circle(Mat& m)
{
    Vec2f center(m.cols/2, m.rows/2);
    for(float i = 0; i < m.rows; i++)
        for(float j = 0; j < m.cols; j++)
        {
            Vec2f pos(j,i);
            if(norm(pos, center) > m.cols/2)
                m.at<Vec3b>(i,j) = Vec3b(0x00,0xff,0x00);
        }
}


Mat HUDManager::create_compass(int yaw_degrees, Point2f local_goal)
{
    while(yaw_degrees > 180) yaw_degrees -= 360;
    while(yaw_degrees < -180) yaw_degrees += 360;

    /*
        North: -90 < y < 90
        East: 0 < y < 180
        West: 0 > y > -180
        South: -90 > y > -180 OR 90 < y < 180 
    */

    Mat result(_bar.size(), CV_8UC3, Scalar(0x55,0x55,0x55));
    insert_at(_bar, result, result.cols/2, result.rows/2, Vec3b(0xff, 0xff, 0xff), Vec3b(0,0,0));
    int dist_to_north = abs(yaw_degrees);
    int dist_to_east = abs(yaw_degrees - 90);
    int dist_to_west = abs(yaw_degrees + 90);
    int dist_to_south = min(abs(yaw_degrees - 180),abs(yaw_degrees+180));
    //printf("[%d] %dN\t%dE\t%dW\t%dS\n", yaw_degrees, dist_to_north, dist_to_east, dist_to_west, dist_to_south);
    if(dist_to_north < 90)
    {
      //  printf("N\n");
        double ratio = yaw_degrees / 90.0;
        Point pos(_bar.cols/2 - _bar.cols/2*ratio, result.rows-12);
        //insert_at(_selector, result, pos.x+8, pos.y-6, Vec3b(0xff, 0xff, 0xff), Vec3b(0,0,0));
        putText(result, "N", pos,  FONT_HERSHEY_SIMPLEX, 0.7, Scalar(0xff,0xe1,0), 2);
    }
    if(dist_to_east < 90)
    {
     //   printf("E\n");
        double ratio = (90-yaw_degrees)/ 90.0;
        Point pos(_bar.cols/2 + _bar.cols/2*ratio, result.rows-12);
        //insert_at(_selector, result, pos.x+8, pos.y-6, Vec3b(0xff, 0xff, 0xff), Vec3b(0,0,0));
        putText(result, "E", pos,  FONT_HERSHEY_SIMPLEX, 0.7, Scalar(0xff,0xe1,0), 2);
    }
    if(dist_to_west < 90)
    {
        double ratio = -(yaw_degrees+90)/ 90.0;
       // printf("W %f\n", ratio);
        Point pos(_bar.cols/2 + _bar.cols/2*ratio, result.rows-12);
        //insert_at(_selector, result, pos.x+8, pos.y-6, Vec3b(0xff, 0xff, 0xff), Vec3b(0,0,0));
        putText(result, "W", pos,  FONT_HERSHEY_SIMPLEX, 0.7, Scalar(0xff,0xe1,0), 2);
    }
    if(dist_to_south < 90)
    {
        //printf("S\n");
        double ratio = (yaw_degrees > 0 ? (180 - yaw_degrees) : (-180 - yaw_degrees)) / 90.0;
        Point pos(_bar.cols/2 + _bar.cols/2*ratio, result.rows-12);
        //insert_at(_selector, result, pos.x+8, pos.y-6, Vec3b(0xff, 0xff, 0xff), Vec3b(0,0,0));
        putText(result, "S", pos,  FONT_HERSHEY_SIMPLEX, 0.7, Scalar(0xff,0xe1,0), 2);
    }

    if(local_goal.x || local_goal.y)
    {
        double alpha = atan2(local_goal.y, local_goal.x);
        if(abs(alpha)<M_PI/2)
        {
            double ratio = alpha/M_PI*2;
            Point pos(_bar.cols/2 - _bar.cols/2*ratio, result.rows-12);
            putText(result, "[+]", pos,  FONT_HERSHEY_SIMPLEX, 0.7, Scalar(0xA0,0xA0,0xff), 2);
        }
    }


    return result;
}
