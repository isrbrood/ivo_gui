#include "netlib.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>
#include <exception>
#include "Exception.hpp"



uint8_t Packet::packet_counter;

PacketPtr Packet::make(uint payload_size)
{
    char* mem = new char[sizeof(Packet) + payload_size];
    return PacketPtr(reinterpret_cast<Packet*>(mem));
}

PacketPtr Packet::make(PacketType type, void* data, uint payload_size)
{
    char* mem = new char[sizeof(Packet) + payload_size];
    auto ptr = PacketPtr(reinterpret_cast<Packet*>(mem));
    ptr->type = type;
    ptr->version = CURRENT_NETLIB_VERSION;
    memcpy(&ptr->data, data, payload_size);
    return ptr;
}

std::vector<uint8_t> Packet::get_data(uint total_size) const
{
    std::vector<uint8_t> v(total_size-sizeof(Packet));
    memcpy(v.data(), &data, v.size());
    return v;
}


Connection::Connection(u_short port) {
    dest_addr = {
        AF_INET,
        0,
        inet_addr("0.0.0.0"),
        {}
    };
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
        throw Exception("Failed to create UDP socket");
    struct sockaddr_in name {
        AF_INET,
        htons(port),
        INADDR_ANY,
        {}
    };
    if (bind(sockfd, (struct sockaddr*)&name, sizeof(name)) < 0)
        throw Exception("Socket bind() failed");
}

Connection::~Connection() {
    close(sockfd);
}

void Connection::connect(const std::string &dest, u_short port) {
    dest_addr.sin_port = htons(port);
    dest_addr.sin_addr = {inet_addr(dest.c_str())};
}

void Connection::send(const Packet &data, uint data_size){
    Packet::packet_counter++;
    sendto(sockfd, &data, sizeof(Packet)+data_size, 0, (sockaddr*)(&dest_addr), sizeof(dest_addr));
}

void Connection::send(const std::vector<u_char> &data) {
    Packet::packet_counter++;
    sendto(sockfd, data.data(), data.size(), 0, (sockaddr*)(&dest_addr), sizeof(dest_addr));
}

void Connection::receive(std::vector<u_char> &data) {

    ssize_t len = recvfrom(sockfd, 0, 0, MSG_TRUNC | MSG_PEEK, 0, 0);

    if (len < 0)
        throw Exception("Socket error in recvfrom()");
    data.resize(len);
    socklen_t addrlen = sizeof(dest_addr);

    recvfrom(sockfd, data.data(), len, 0, (sockaddr*)(&dest_addr), &addrlen);
}

bool Connection::is_connected() const
{
    struct sockaddr_in zero_struct =
    {
        AF_INET,
        0,
        inet_addr("0.0.0.0")
    };
    return 0 != memcmp((const void*)&dest_addr, (const void*)&zero_struct, sizeof(struct sockaddr_in));
}
